#!/usr/bin/env python3

import numpy as np
import cv2

MIN_MATCH_COUNT = 10
MATCH_THRESHOLDS = {
    'min': .7,
    'max': .1
}
FLANN_INDEX_KDTREE = 0

sift = cv2.xfeatures2d.SIFT_create()


#def remove_transparency(source, background_color):
#    source_img = cv2.cvtColor(source[:,:,:3], cv2.COLOR_BGR2GRAY)
#    source_mask = source[:,:,3] * (1 / 255.0)
#
#    background_mask = 1.0 - source_mask
#
#    bg_part = (background_color * (1 / 255.0)) * (background_mask)
#    source_part = (source_img * (1 / 255.0)) * (source_mask)
#
#    return np.uint8(cv2.addWeighted(bg_part, 255.0, source_part, 255.0, 0.0))


def match_points(img, alpha):
    kp1, des1 = sift.detectAndCompute(img, None)
    kp2, des2 = sift.detectAndCompute(alpha, None)

    #m = alpha.copy()
    #out_alpha = cv2.drawKeypoints(alpha, kp2, m)
    #cv2.imwrite('alpha_kp.png', out_alpha)

    #out_img = cv2.drawKeypoints(img, kp1, m)
    #cv2.imwrite('img_kp.png', out_img)

    index_params = {
        'algorithm': FLANN_INDEX_KDTREE,
        'trees': 5
    }
    search_params = { 'checks': 50 }
    matcher = cv2.FlannBasedMatcher(index_params, search_params)
    #matcher = cv2.BFMatcher()
    matches = matcher.knnMatch(des1, des2, k=2)
    #matches = matcher.knnMatch(des2, des1, k=2)
    #print(len(matches))
    #good_matches, raw_good_matches, mask = filter_matches(matches)
    #print(mask)

    g = []
    matchesMask = [ [0, 0] for i in range(len(matches)) ]
    for i, (m, n) in enumerate(matches):
        if  .6 * n.distance < m.distance < .85 * n.distance:
            matchesMask[i] = [1, 0]
            g.append((m.trainIdx, m.queryIdx))
    good_matches = g
    if len(good_matches) < MIN_MATCH_COUNT:
        return None, None
    #print('goods', len(g))

    #draw_params = {
    #    'matchesMask': matchesMask,
    #    'matchColor': (0, 255, 0),
    #    'singlePointColor': (255, 0, 0),
    #    'flags': 2
    #}
    #matches_img = cv2.drawMatchesKnn(
    #    alpha,
    #    kp2,
    #    img,
    #    kp1,
    #    matches,
    #    None,
    #    **draw_params
    #)
    #matches_img = cv2.drawMatchesKnn(
    #    alpha,
    #    kp2,
    #    img,
    #    kp1,
    #    good_matches,
    #    None,
    #    flags=2
    #)
    #cv2.imwrite('good-matches.png', matches_img)

    src = np.float32(list(kp2[i].pt for (i, _) in good_matches))
    dst = np.float32(list(kp1[i].pt for (_, i) in good_matches))
    return src, dst


def generate_trimap(alpha):
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    fg = np.array(np.equal(alpha, 255).astype(np.float32))
    # fg = cv.erode(fg, kernel, iterations=np.random.randint(1, 3))
    unknown = np.array(np.not_equal(alpha, 0).astype(np.float32))
    unknown = cv2.dilate(unknown, kernel, iterations=np.random.randint(1, 20))
    trimap = fg * 255 + (unknown - fg) * 128
    return trimap.astype(np.uint8)


def main(args):
    img = cv2.imread(args.image)
    alpha = cv2.imread(args.alpha, cv2.IMREAD_UNCHANGED)
    # TODO: use this as a mask
    #a1 = remove_transparency(alpha, 0)
    #cv2.imwrite('remove-trans-test.png', a1)
    #print(alpha[:,:,:3].shape)
    b, g, r, a = cv2.split(alpha)
    alpha = cv2.merge((b, g, r))
    alpha[a < 127] = 0

    src_points, dst_points = match_points(img, alpha)
    H, _ = cv2.findHomography(src_points, dst_points, cv2.RANSAC)

    h, w, _ = img.shape
    warped_alpha = cv2.warpPerspective(alpha, H, (w, h))
    cv2.imwrite(args.output_alpha, warped_alpha)

    matting = cv2.warpPerspective(a, H, (w, h))
    cv2.imwrite(args.output_matting, matting)

    if args.output_trimap:
        trimap = generate_trimap(matting)
        cv2.imwrite(args.output_trimap, trimap)


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-i', '--image', type=str, required=True, help='input image')
    parser.add_argument('-a', '--alpha', type=str, required=True, help='input alpha')
    parser.add_argument('-oa', '--output-alpha', type=str, required=True, help='output alpha image')
    parser.add_argument('-om', '--output-matting', type=str, required=True, help='output matting image')
    parser.add_argument('-ot', '--output-trimap', type=str, required=False, help='output trimap image')
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())
