#!/bin/bash

CODE_MOUNTPOINT=${1}

docker run --runtime=nvidia \
	--mount type=bind,source=${CODE_MOUNTPOINT},target=/code \
	-it jjanzic/docker-python3-opencv:contrib-opencv-3.2.0 bash
